; download dependencies for the D7 Scratch Theming Install Profile
core = 7.x

api = 2
projects[] = "drupal"

projects[ctools][type] = "module"
projects[ctools][subdir] = "contrib/"

projects[context][version] = 3.0-beta6
projects[context][type] = "module"
projects[context][subdir] = "contrib/"

projects[module_filter][version] = 1.7
projects[module_filter][type] = "module"
projects[module_filter][subdir] = "contrib/"

projects[backup_migrate][version] = 2.4
projects[backup_migrate][type] = "module"
projects[backup_migrate][subdir] = "contrib/"

projects[coffee][type] = "module"
projects[coffee][subdir] = "contrib/"

projects[admin_menu][type] = "module"
projects[admin_menu][subdir] = "contrib/"

projects[devel][type] = "module"
projects[devel][subdir] = "contrib/"

projects[features][type] = "module"
projects[features][subdir] = "contrib/"

projects[strongarm][type] = "module"
projects[strongarm][subdir] = "contrib/"